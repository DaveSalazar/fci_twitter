<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/maps', 'MapsController@index')->name('index');
Route::get('/ventana2', 'MapsController@ventana2')->name('ventana2');
Route::get('/ventana3', 'MapsController@ventana3')->name('ventana3');
Route::get('/maps/zonas/crear', 'MapsController@createZona')->name('crearZona');
Route::post('/maps/zona/guardar', 'MapsController@storeZona')->name('guardarZona');
Route::get('/twitter', 'TwitterController@index');
Route::post('/ventana3', 'MapsController@ventana3Post')->name('ventana3Post');
