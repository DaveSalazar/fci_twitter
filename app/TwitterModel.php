<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Twitter;

class TwitterModel extends Model
{
    
   
    public static function busqueda($var, $geo){
        if($geo == null)
            $geocode = Configuracion::where('nombre', 'TWITTER_GEOCODE')->first()->valor;
        else {
            $geocode = $geo;
        }    
        //dd($geocode);
        $array = [  
                    'q' => $var, 
                    'geocode' => $geocode,
                    'format' => 'array',
                    'result_type'=>'recent', 
                    'count' => 99
                 ];
        
        $followers = Twitter::getSearch($array);

        $seguidores=($followers['statuses']);

        return $seguidores;
    }

    public static function filtrarRT($datos){
        
        $final = [];
        
        for($i=0;$i<count($datos);$i++){
            //Filtro de RT
            if( strpos($datos[$i]['text'], 'RT')  === false )
                array_push($final, $datos[$i]);
        }
            
                
        return $final;
    }

    public static function getEstadisticasTweetsFiltrados($palabras, $geo){
        
        
        $sentiment = new \PHPInsight\Sentiment();

        $tweets = [];
        $arrPalabras = [];

        for($i=0;$i<count($palabras);$i++){            
            $busqueda = TwitterModel::busqueda($palabras[$i],$geo);
            $filtro = TwitterModel::filtrarRT($busqueda);
            $tweets += $filtro;

            $arrPalabras[$palabras[$i]]['data'] = $filtro;
        }      
        
        $arr = [];
        $arr['cantPositivos'] = 0;
        $arr['cantNegativos'] = 0;
        $arr['cantNeutrales'] = 0;
        foreach($palabras as $palabra){
            $arr[$palabra] = [];
            $arr[$palabra]['cantPositivos'] = 0;
            $arr[$palabra]['cantNegativos'] = 0;
            $arr[$palabra]['cantNeutrales'] = 0;
            
            foreach($arrPalabras[$palabra]['data'] as $follow){

                $sentimiento['data'] = array( 
                                    'text' => $follow['text'], 
                                    'score' => $sentiment->score($follow['text']), 
                                    'class' => $sentiment->categorise($follow['text']) 
                );
    
                switch($sentimiento['data']['class']) {
                    case 'pos':
                        $arr[$palabra]['cantPositivos'] ++;
                        $arr['cantPositivos'] ++;
                    break;
                    case 'neg';
                        $arr[$palabra]['cantNegativos'] ++;
                        $arr['cantNegativos'] ++;
                    break;
                    case 'neu':
                        $arr[$palabra]['cantNeutrales'] ++;
                        $arr['cantNeutrales'] ++;
                    break;
    
                }
                array_push($arr[$palabra], $sentimiento);
                                          
            }
        }
        return $arr;
    }
}
