<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Zona;
use Twitter;
use App\TwitterModel;
use PHPInsight; 
use App\Configuracion;

class MapsController extends Controller
{
    public function index() 
    {
        $zonas = Zona::all();
  
        $palabras = Configuracion::where('nombre', 'PALABRAS_TWITTER')->first()->valor;
        $palabras =  explode(',', $palabras);
        $arr = TwitterModel::getEstadisticasTweetsFiltrados($palabras, null);

        return view('maps.index', compact('zonas', 'arr'));
    }

    public function ventana2(){

        $zonas = Zona::all();
        $zonaReporte = null;
        $coords = [];
        $palabra = [];
        $arr = null;
        if(Input::get('zona') != null && Input::get('palabra') != null){
            $zonaReporte = $zonas->where('id', Input::get('zona'))->first();
            $c = explode(';', $zonaReporte->coordenadas);
            for($i=0; $i<count($c); $i++){      
                $ltLng = explode(',', $c[$i]);
                if(count($ltLng)==2)
                    array_push($coords, array( 'lat' => $ltLng[0], 'lng' => $ltLng[1]) );
            }
            array_push($palabra, Input::get('palabra'));
            $arr = TwitterModel::getEstadisticasTweetsFiltrados($palabra,null);
           
        }
            
        $palabras = Configuracion::where('nombre', 'PALABRAS_TWITTER')->first()->valor;
        $palabras =  explode(',', $palabras);
        //dd($coords);
        return view('maps.ventana2', compact('zonas', 'palabra', 'zonaReporte', 'palabras', 'arr'));
    }

    public function ventana3(){
        $zonas = Zona::all();
        return view('maps.ventana3', compact('zonas'));
    }

    public function ventana3Post(Request $request){
        
        $zonas = Zona::all();
  
        $palabras = Configuracion::where('nombre', 'PALABRAS_TWITTER')->first()->valor;
        $palabras =  explode(',', $palabras);
        $geo = $request->lat.",".$request->lng.",".$request->kmRadius."km";
        $arr = TwitterModel::getEstadisticasTweetsFiltrados($palabras, $geo);

        return $arr;
    }

    public function createZona()
    {
        $zonas = Zona::all();
        return view('welcome', compact('zonas'));
    }

    public function storeZona(Request $request) 
    {

        //dd($request->all());
        foreach($request->all() as $obj ){
            //dd($obj);
            $zona = new Zona();
            $zona->coordenadas = "";               
               
            for($j = 0; $j < count($obj['latLngs'][0]); $j++)
                $zona->coordenadas = $zona->coordenadas.$obj['latLngs'][0][$j]['lat'].",".$obj['latLngs'][0][$j]['lng'].";";
            
            $zona->nombre = $obj['nombre'];                   
            $zona->save();   
        }
        

        return response()->json([
            'msg' => 'zona creada' 
        ], 201);
    }
}
