@extends('layouts.master') 
@section('styles')
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
   integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
   crossorigin=""/>
<style>
    /* Always set the map height explicitly to define the size of the div
    * element that contains the map. */
    #mapid { height: 800px; width:100%}
</style>

@endsection

@section('content')
    <h1 align="center">Analisis de sentimiento en twitter</h1>
    <h1 align="center">Guayaquil</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div id="mapid"></div>        
            </div>
        </div>
        <br>
        <div class="row">
            <a href="{{ route('crearZona') }}" class="btn btn-primary">Crear zona</a>
        <br>
        </div>
        <div class="row">
            <div class="col-lg-6" style="height:100%;">
                <h4 align="center">REPORTE DE  ANALISIS DE</h4>
                <h4 align="center">SENTIMIENTO GENERAL</h4>
                <canvas id="bar-chart" width="800" height="450"></canvas>
            </div> 
            <div class="col-lg-6">
                <table class="table-responsive">                
                @foreach($arr as $key => $palabra)
                @if($key != 'cantPositivos' && $key != 'cantNegativos' && $key != 'cantNeutrales')
                <td>
                    <h4 align="center">REPORTE DE  ANALISIS DE</h4>
                    <h4 align="center">SENTIMIENTO GENERAL</h4>
                    <h3 align="center">{{ $key }}</h3>
                    <canvas id="pie-chart{{ $key }}" width="800" height="450"></canvas>
                </td>                
                @endif
                @endforeach
                </table>
            </div> 
        </div>
    </div>    

@endsection

@section('scripts')
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
   integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
   crossorigin=""></script>
    <script>
        
        var mymap = L.map('mapid').setView([-2.1880232, -79.9303973], 13);
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoiZGF2ZS1zYWxhemFyIiwiYSI6ImNqcTJtNW5zaDE5cno0M3M3cGY1eWc2NDkifQ.NKsu3DL1RFWfrsn-4BWouQ'
        }).addTo(mymap);

        @foreach($zonas as $zona)
        var polygon = L.polygon(
            [            
                @php
                    $coords = explode(';', $zona->coordenadas);
                    for($i=0; $i<count($coords); $i++){                    
                        $ltLng = explode(',', $coords[$i]);
                        if(count($ltLng)==2)
                            echo "[{$ltLng[0]}, {$ltLng[1]}],";
                    }
                @endphp            
            ],
            {

            }
        ).bindTooltip('{{ $zona->nombre }}').addTo(mymap);
        @endforeach


        new Chart(document.getElementById("bar-chart"), {
            type: 'bar',
            data: {
                labels: ["Positivo", "Negativo", "Neutral"],
                datasets: [{
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                        data: [
                            {{ $arr['cantPositivos'] }},
                            {{ $arr['cantNegativos'] }},
                            {{ $arr['cantNeutrales'] }}
                        ]                    
                }]
            },
            options: {
                legend: { 
                    display: false 
                    },
                title: {
                    display: true,
                    text: 'Estadisticas de sentimiento (Cantidad)'
                }
            }
        });

        @foreach($arr as $key => $palabra)
        @if($key != 'cantPositivos' && $key != 'cantNegativos' && $key != 'cantNeutrales')
            new Chart(document.getElementById("pie-chart{{ $key }}"), {
                type: 'pie',
                data: {
                    labels: ["Positivo", "negativo", "neutral"],
                    datasets: [{
                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                        data: [
                                {{ $palabra['cantPositivos'] }},
                                {{ $palabra['cantNegativos'] }},
                                {{ $palabra['cantNeutrales'] }}
                            ]
                            
                    }]
                },
                options: {
                title: {
                    display: true,
                    text: 'Estadisticas de sentimiento (Cantidad)'
                }
                }
            });
            @endif
        @endforeach
    </script>

@endsection