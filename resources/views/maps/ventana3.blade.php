@extends('layouts.master') 
@section('styles')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
    integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
    crossorigin=""/>
    <link rel="stylesheet" href="https://unpkg.com/leaflet-draw@1.0.2/dist/leaflet.draw.css" />
    <style>         
     #mapid { height: 400px; width:100%}

	</style> 
@endsection
@section('content')
    <h1 align="center">Open Street Maps</h1>
    <hr>
    <div class="container">
        <div class="row">          
            <div class="col-lg-12">
                <div id="mapid"></div>                      
            </div>
            <div class="col-lg-6">
                <input type="text" name="coordenadas" id="coordsZona" value="" style="visibility: hidden;">
                <input type="text" name="nombre"  style="visibility: hidden;">
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6" style="height:100%;">
            <h4 align="center">REPORTE DE  ANALISIS DE</h4>
            <h4 align="center">SENTIMIENTO GENERAL</h4>
            <canvas id="bar-chart" width="800" height="450"></canvas>
        </div> 
        <div class="col-lg-6">
            <table id="tblEstadisticas" class="table-responsive"> 
                               
            </table>
        </div> 
    </div>
</div>    
@endsection

@section('scripts')
<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
   integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
   crossorigin=""></script>
   <script src="https://unpkg.com/leaflet-draw@1.0.2/dist/leaflet.draw.js"></script>
    <script>
        $('#frmNombre').submit(function (e) {
            e.preventDefault()
            $("#myModal").modal('hide');
        })
        var map = L.map('mapid').setView([-2.1880232, -79.9303973], 13);
            L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'pk.eyJ1IjoiZGF2ZS1zYWxhemFyIiwiYSI6ImNqcTJtNW5zaDE5cno0M3M3cGY1eWc2NDkifQ.NKsu3DL1RFWfrsn-4BWouQ'
        }).addTo(map);
            
        // Initialise the FeatureGroup to store editable layers
        var drawnItems = new L.FeatureGroup();
        map.addLayer(drawnItems);
        // Initialise the draw control and pass it the FeatureGroup of editable layers
        var drawControl = new L.Control.Draw({
            
            edit: {
                featureGroup: drawnItems
            },
            draw: {
                polyline : false,
                rectangle : false,
                circle : true,
                marker: false,
                circlemarker: false,
                polygon: false
            }
        });

        map.addControl(drawControl);

        map.on(L.Draw.Event.CREATED, function (e) {
           
            var type = e.layerType
            var layer = e.layer;      
            drawnItems.addLayer(layer);   
            drawControl.remove();
            var data = {
                lat: layer._latlng.lat,
                lng: layer._latlng.lng,
                kmRadius: layer._mRadius*0.001
            }

            $.ajax({
                url: '{{ route('ventana3Post') }}',
                dataType: 'json',
                type: 'post',
                contentType: 'application/json',
                data: JSON.stringify(data),
                processData: false,
                success: ( data, textStatus, jQxhr ) => {
                    // GRAFICO DE Barras
                    new Chart(document.getElementById("bar-chart"), {
                        type: 'bar',
                        data: {
                            labels: ["Positivo", "Negativo", "Neutral"],
                            datasets: [{
                                    backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                                    data: [
                                        data.cantPositivos,
                                        data.cantNegativos,
                                        data.cantNeutrales
                                    ]                    
                            }]
                        },
                        options: {
                            legend: { 
                                display: false 
                                },
                            title: {
                                display: true,
                                text: 'Estadisticas de sentimiento (Cantidad)'
                            }
                        }
                    });
                    //GRAFICO DE PASTEL
                    for( el in data) {
                        console.log(data[el]);
                       if(el !== "cantPositivos" && el !== "cantNegativos" && el !== "cantNeutrales" ){
                            $('#tblEstadisticas').append(` 
                                <td>
                                    <h4 align="center">REPORTE DE  ANALISIS DE</h4>
                                    <h4 align="center">SENTIMIENTO GENERAL</h4>
                                    <h3 align="center">${el}</h3>
                                    <canvas id="pie-chart${el}" width="800" height="450"></canvas>
                                </td>
                            `);
                            new Chart(document.getElementById(`pie-chart${el}`), {
                                type: 'pie',
                                data: {
                                    labels: ["Positivo", "negativo", "neutral"],
                                    datasets: [{
                                        backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f"],
                                        data: [
                                                data[el].cantPositivos,
                                                data[el].cantNegativos,
                                                data[el].cantNeutrales
                                            ]                                            
                                    }]
                                },
                                options: {
                                title: {
                                    display: true,
                                    text: 'Estadisticas de sentimiento (Cantidad)'
                                }
                                }
                            });
                       }
                    }
                },
                error: function( jqXhr, textStatus, errorThrown ){
                    
                }
            });
        });

       
    </script>
@endsection